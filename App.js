/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component, useState} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import {
    RTCPeerConnection,
    RTCIceCandidate,
    RTCSessionDescription,
    RTCView,
    MediaStream,
    MediaStreamTrack,
    mediaDevices
} from 'react-native-webrtc';

//https://react-native-webrtc.discourse.group/t/streamurl-not-getting-set/46
export default class App extends Component{
    constructor(props){
        super(props);
        this.state = {streamUrl: null}
    }

    getLocalStream = async (isFront) =>{
        debugger
        console.log('getLocalStream');
        let sourceInfos = await mediaDevices.enumerateDevices();
        let videoSource = sourceInfos.filter((sourceInfo)=>{
            return sourceInfo.kind.includes("video") && sourceInfo.facing === (isFront?"front":"back")
        })[0];
        let videoSourceId = videoSource.deviceId;
        let userDeviceStream = await mediaDevices.getUserMedia({audio: true,
            video: {
                mandatory: {
                    minWidth: 640, // Provide your own width, height and frame rate here
                    minHeight: 360,
                    minFrameRate: 30,
                },
                facingMode: (isFront? "user":"environment"),
                optional: (videoSourceId?[{sourceId: videoSourceId}]:[])
            }});
        let streamUrl = userDeviceStream.toURL();
        debugger
        return new Promise(resolve => {
            resolve(streamUrl);
        })
    };

    componentDidMount(){
        console.log('componentDidMount');
        this.getLocalStream(true)
            .then(streamUrl=>{
                debugger;
                this.setState({...this.state, streamUrl: streamUrl})
            });
    }
//{this.state.streamUrl && <RTCView streamUrl={this.state.streamUrl} objectFit="cover" zOrder={10} />}
    render(){
        debugger;
        return(
            <View>
                <Text>Teste Web RTC 2 - O pesadelo final</Text>
                <View style={{backgroundColor:'red', flex:1}}>
                    {this.state.streamUrl && <RTCView streamUrl={this.state.streamUrl} style={{width:250, height:250}} />}
                </View>
            </View>
        )
    }
}

///ref={ handle=>{
//                          if(handle){
//                              this.component = handle.parentNode;
//                              this.svg = d3.select(handle);
//                              this.bounds = handle?handle.getBoundingClientRect():this.bounds;
//                          }

// export default function App() {
//     debugger;
//     const [myStream, setMyStream] = useState(null);
//     if(!myStream) {
//         const configuration = {"iceServers": [{"url": "stun:stun.l.google.com:19302"}]};
//         const pc = new RTCPeerConnection(configuration);
//
//         let isFront = true;
//         mediaDevices.enumerateDevices().then(sourceInfos => {
//             console.log(sourceInfos);
//             let videoSourceId;
//             for (let i = 0; i < sourceInfos.length; i++) {
//                 const sourceInfo = sourceInfos[i];
//                 if (sourceInfo.kind === "videoinput" && sourceInfo.facing === (isFront ? "front" : "back")) {
//                     videoSourceId = sourceInfo.deviceId;
//                 }
//             }
//             mediaDevices.getUserMedia({
//                 audio: true,
//                 video: {
//                     mandatory: {
//                         minWidth: 500, // Provide your own width, height and frame rate here
//                         minHeight: 300,
//                         minFrameRate: 30
//                     },
//                     facingMode: (isFront ? "user" : "environment"),
//                     optional: (videoSourceId ? [{sourceId: videoSourceId}] : [])
//                 }
//             }).then(stream => {
//                 setMyStream(stream);
//             }).catch(error => {
//                 console.error(error)
//             });
//         });
//
//         pc.createOffer().then(desc => {
//             pc.setLocalDescription(desc).then(() => {
//                 // Send pc.localDescription to peer
//             });
//         });
//
//         pc.onicecandidate = function (event) {
//             // send event.candidate to peer
//         };
//     }
//
//     return (
//         <View>
//             <Text>Teste de Web RTC</Text>
//             {myStream && <RTCView streamURL={myStream.toURL()}/>}
//         </View>
//     );
// };


